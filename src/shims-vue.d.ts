declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}
//
// import {Store} from "vuex";
// import {KimchiRootState} from "@/store";
// import {Dialog, LoadingProgrammatic, ModalProgrammatic, Snackbar, Toast} from "buefy";
//
// declare module 'vue/types/vue' {
//     interface Vue {
//         $store: Store<KimchiRootState>;
//         $dialog: typeof Dialog;
//         $loading: typeof LoadingProgrammatic;
//         $modal: typeof ModalProgrammatic;
//         $snackbar: typeof Snackbar;
//         $toast: typeof Toast;
//     }
// }
