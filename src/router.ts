import Vue from 'vue';
import Router from 'vue-router';
import Landing from './views/Landing.vue';
import Login from './views/Login.vue';
import Timesheet from './views/Timesheet.vue';
import store from "./store";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'landing',
            component: Landing
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/home',
            name: 'home',
            meta: {
                loginRequired: true
            }
        },
        {
            path: '/times',
            name: 'timesheet',
            meta: {
                loginRequired: true
            },
            component: Timesheet
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.loginRequired)) {
        if (!store.state.auth.authToken) {
            next({name: 'login', query: {'redirectTo': to.fullPath}});
            return;
        }
    }

    next();
});

export default router;
