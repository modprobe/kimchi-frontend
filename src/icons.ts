import {default as far} from '@fortawesome/fontawesome-pro-regular';
import {default as fal} from '@fortawesome/fontawesome-pro-light';
import {library} from "@fortawesome/fontawesome-svg-core";
import Vue from "vue";

// @ts-ignore: No definitions yet
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';

// @ts-ignore
library.add(fal, far);
Vue.component('fa-icon', FontAwesomeIcon);