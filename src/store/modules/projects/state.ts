import {Project} from "@/types/projects";

export interface KimchiProjectState {
    availableProjects: Project[];
    currentProject: Project|null;
}

const state: KimchiProjectState = {
    availableProjects: [],
    currentProject: null,
};

export default state;