import {MutationTree} from "vuex";
import {KimchiProjectState} from "@/store/modules/projects/state";
import {Project} from "@/types/projects";

const mutations: MutationTree<KimchiProjectState> = {
    setAvailableProjects(state, payload) {
        state.availableProjects = payload;
    },
    setCurrentProject(state, project: Project) {
        state.currentProject = project;
    }
};

export default mutations;
