import state, {KimchiProjectState} from './state';
import mutations from './mutations';
import actions from './actions';
import {Module} from "vuex";
import {KimchiRootState} from "@/store";

const projectModule: Module<KimchiProjectState, KimchiRootState> = {
    namespaced: true,
    state,
    mutations,
    actions
};

export default projectModule;