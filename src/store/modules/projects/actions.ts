import {ActionTree} from "vuex";
import {KimchiRootState} from "@/store";
import {KimchiProjectState} from "@/store/modules/projects/state";
import ApiService from "@/services/api.service";

const actions: ActionTree<KimchiProjectState, KimchiRootState> = {
    loadAvailableProjects(context) {
        return ApiService.listProjects()
            .then(response => context.commit('setAvailableProjects', response.data))
    }
};

export default actions;