export const SET_AUTH_TOKEN = 'setAuthToken';
export const LOGOUT = 'logout';
export const SET_CURRENT_USER = 'setCurrentUser';

export default {
    SET_AUTH_TOKEN: `auth/${SET_AUTH_TOKEN}`,
    LOGOUT: `auth/${LOGOUT}`,
    SET_CURRENT_USER: `auth/${SET_CURRENT_USER}`
}

