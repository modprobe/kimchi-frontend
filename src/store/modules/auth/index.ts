import state, {KimchiAuthState} from './state';
import mutations from './mutations';
import actions from './actions';
import {Module} from "vuex";
import {KimchiRootState} from "@/store";


const authModule: Module<KimchiAuthState, KimchiRootState> = {
    namespaced: true,
    state,
    mutations,
    actions
};

export default authModule;