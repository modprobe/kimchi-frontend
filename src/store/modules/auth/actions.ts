import {ActionTree} from "vuex";
import {KimchiAuthState} from "@/store/modules/auth/state";
import {KimchiRootState} from "@/store";
import {Credentials} from "@/types/auth";
import {SET_AUTH_TOKEN, SET_CURRENT_USER} from './mutation-types';
import ApiService from "@/services/api.service";

const actions: ActionTree<KimchiAuthState, KimchiRootState> = {
    login(context, credentials: Credentials) {
        return ApiService
            .authenticate(credentials.username, credentials.password)
            .then(response => context.commit(SET_AUTH_TOKEN, response.data.token))
            .then(() => context.commit(SET_CURRENT_USER, credentials.username))
    }
};

export default actions;