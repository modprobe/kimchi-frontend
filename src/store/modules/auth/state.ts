export declare interface KimchiAuthState {
    authToken: string|null;
    currentUser: string|null;
}

export default {
    authToken: null,
    currentUser: null,
};