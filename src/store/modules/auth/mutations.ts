import {LOGOUT, SET_AUTH_TOKEN, SET_CURRENT_USER} from "@/store/modules/auth/mutation-types";
import {KimchiAuthState} from "@/store/modules/auth/state";
import {MutationTree} from "vuex";

const mutations: MutationTree<KimchiAuthState> = {
    [SET_AUTH_TOKEN] (state: KimchiAuthState, authToken: string) {
        state.authToken = authToken;
    },
    [LOGOUT] (state: KimchiAuthState) {
        state.authToken = null;
        state.currentUser = null;
    },
    [SET_CURRENT_USER] (state: KimchiAuthState, username: string) {
        state.currentUser = username;
    }
};

export default mutations;
