import Vue from 'vue';
import Vuex, {Store} from 'vuex';
import VuexPersist from "vuex-persist";

import auth from './modules/auth';
import projects from './modules/projects';
import {KimchiAuthState} from './modules/auth/state';
import {KimchiProjectState} from "@/store/modules/projects/state";

Vue.use(Vuex);

export interface KimchiRootState {
    auth: KimchiAuthState,
    projects: KimchiProjectState,
}

const vuexLocalStorage = new VuexPersist({
    key: 'vuex',
    storage: window.localStorage,
    modules: [
        'auth'
    ]
});

const store: Store<KimchiRootState> = new Vuex.Store<KimchiRootState>({
    modules: {
        auth,
        projects
    },
    plugins: [vuexLocalStorage.plugin]
});

export default store;
