import axios, {AxiosInstance} from 'axios';
import store from '../store';
import router from '../router';
import authMutations from "@/store/modules/auth/mutation-types";
import swal from 'sweetalert2';

export default class ApiService {
    static getClient(addInterceptor: boolean = true): AxiosInstance {
        let client = axios.create({
            baseURL: process.env.VUE_APP_KIMCHI_BASE_URL,
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (store.state.auth.authToken) {
            client.defaults.headers['Authorization'] = `Bearer ${store.state.auth.authToken}`;
        }

        if (addInterceptor) {
            client.interceptors.response.use(response => response, error => {
                if (error.response.status === 401) {
                    swal({
                        title: 'Session expired',
                        text: 'Your session has expired. You will be redirected to the login page.',
                        type: 'error',
                    }).then(() => router.replace('/login'));
                } else {
                    return Promise.reject(error);
                }
            });
        }

        return client;
    }

    static authenticate(username: string, password: string) {
        return this.getClient(false)
            .post(
                '/auth/login',
                JSON.stringify({
                    username: username,
                    password: password
                })
            );
    }

    static listProjects() {
        return this.getClient()
            .get('/project');
    }
}