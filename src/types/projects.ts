import {Company} from "@/types/companies";

export interface Project {
    id: number;
    name: string;
    company?: Company|number
}