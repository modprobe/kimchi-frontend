// A custom Nightwatch assertion.
// The assertion name is the filename.
// Example usage:
//
//   browser.assert.elementCount(selector, count)
//
// For more information on custom assertions see:
// http://nightwatchjs.org/guide#writing-custom-assertions

exports.assertion = function elementCount(selector: string, count: number) {
    this.message = `Testing if element <${selector}> has count: ${count}`;
    this.expected = count;
    this.pass = (val: number) => val === count;
    this.value = (res: any) => res.value;

    function evaluator(_selector: string) {
        return document.querySelectorAll(_selector).length;
    }

    this.command = (cb: Function) => this.api.execute(evaluator, [selector], cb);
};
